# OWASP_ZAP-Kali Linux 2020.1
```
https://www.zaproxy.org/download/
```
![](https://github.com/nu11secur1ty/OWASP_ZAP/blob/master/wall/owasp_logo_milan.png)

# Online Installer
```bash
curl -s https://raw.githubusercontent.com/nu11secur1ty/OWASP_ZAP/master/zap.sh | bash
```
https://owasp.org/
